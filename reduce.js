function reduce(elements, cb, startingValue){
    if(startingValue!=undefined){
        if(elements.length==0){
            return cb(startingValue);
        }
        cb(startingValue);
    }
    for(let i=0; i<elements.length; i++){
        if(elements.length-1==i){
            return cb(elements[i]);
        }
        cb(elements[i]);
    }
}
module.exports = reduce;