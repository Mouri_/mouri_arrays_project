let result =[];
function flatten(elements){
    for(let i=0; i<elements.length; i++){
        if(Array.isArray(elements[i])){
            flatten(elements[i]);
        }
        else{
            result.push(elements[i]);
        }
    }
    return result;
}
module.exports = flatten;
